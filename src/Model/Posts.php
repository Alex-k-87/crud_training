<?php

namespace App\Model;

final class Posts extends Model
{
    protected $id;
    protected $name;

    public function __construct($id=null, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }
}
