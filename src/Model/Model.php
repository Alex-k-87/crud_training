<?php

namespace App\Model;

abstract class Model
{
    public function insert()
    {
        $table = strtolower(end(explode('\\', static::class)));

        $pdo = new \PDO('mysql:host=mysql;dbname=docker','root','password' );

        $sql = 'INSERT INTO ' . $table . ' (' . implode(', ', array_keys(get_object_vars($this))) .') VALUES' . ' (:' . implode(', :', array_keys(get_object_vars($this))) . ')';

        $stmt = $pdo->prepare($sql);
        foreach (get_object_vars($this) as $field => $value){
            if ($field == 'id' && $value == null) {
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
            continue;
            }
            $stmt->bindValue(':' . $field, $value);
        }
        $stmt->execute();
    }

    public static function find($id)
    {
       $table = strtolower(end(explode('\\', static::class)));

       $pdo = new \PDO('mysql:host=mysql;dbname=docker','root','password' );

        $sql = 'SELECT * FROM ' . $table . ' WHERE id = :id';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(':id', $id);

        $stmt->execute();

        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

            if ($data === false){
                throw new \InvalidArgumentException('Post does not exist');
            }
        return new static($data);
    }

    public function delete(){

        $table = strtolower(end(explode('\\', static::class)));

        $pdo = new \PDO('mysql:host=mysql;dbname=docker','root','password' );

        $sql = 'DELETE FROM ' . $table . ' WHERE id = :id';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(':id', $this->id);

        $stmt->execute();
    }

    public function update()
    {
        $table = strtolower(end(explode('\\', static::class)));

        $pdo = new \PDO('mysql:host=mysql;dbname=docker','root','password' );

        foreach (get_object_vars($this) as $field => $value) {
            if ($field !== 'id' ) {
                $params[] = $field . " = '" . $value . "' ";
            }
        }
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $params) . ' WHERE id = :id';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(':id', $this->id);

        $stmt->execute();

    }
}